from random import randint

# Varibles:
# Int playerWins
#   Counts up whenever the player wins a game
playerWins = 0
# Int computerWins
#   Counts up whenever the computer wins a game
computerWins = 0
# List results stores string type objects
#   contains the result of all interactions in the game, 
#   to add new items add all mesages to this list
#   Format should be Object Verb Object
results = [
    "Scissors cuts Paper",
    "Paper covers Rock",
    "Rock crushes Lizard",
    "Lizard poisons Spock",
    "Spock smashes Scissors",
    "Scissors decapitates Lizard",
    "Lizard eats Paper",
    "Paper disproves Spock",
    "Spock vaporizes Rock",
    "Rock crushes Scissors"
]
# Funtions:
#  Main:
def Main():
#   Import Global varibles
    global playerWins
    global computerWins
#   Varibles:
#   String userInput
#       Used to store user input
    userInput = ""
#   string userItem
#       Stores an item that the user picked
    userItem = ""
#   list result
#       Stores the item the bot picked
    result = []
#   Method:
#   ask user to enter their selection
    print ("\nType your choice of item:")
#   Call input() to get user input and puts output in userInput
    userInput = input()
#   Call ParseInput() on userInput and store in userItem
    userItem = ParseInput(userInput)
    if (userItem == -1):
        return -1
    else:
    #   Call bot() on userItem and store output in result
        result = bot(userItem)
    #   print positions 1-3 of result
        print (result[0] + " " + result[1] + " " + result[2])
    #   case insenitive if position one of result is userItem
        if userItem == result[0]:
    #       increment playerwins
            playerWins += 1
    #   else
        else:
    #       increment computerWins
            computerWins += 1
#  ParseInput:
#   Takes userInput
def ParseInput(userInput):
#   Should return item
#   For loop over results:
    for result in results:
#       store result split by " " in resultSplit
        resultSplit = result.split(" ")
#       If userInput is the first value of resultSplit
        if userInput.lower() == resultSplit[0].lower():
#           return the first value of resultSplit
            return resultSplit[0]
#   return -1
    print ("wrong item mate")
    return -1
#  Bot:
#   Takes string userItem
def bot (userItem):
    
#   Varibles:
#       list posibleResults contains Strings
    possibleResults = []
#   Method:
#   for loop over results:
    for result in results:
#       list currResult is the split current result
        currResult = result.split(" ")
#       if currResult Contains userInput:
        if userItem in currResult:
#           Add currResult to posibleResults
            possibleResults.append(currResult)
#   Pick a random result from posibleResults and return it
    return possibleResults[randint (0,len(possibleResults)-1)]

#print out all posible outcomes
print ("posible outcomes:")
#   Loop over results printing each item
for result in results:
    print ( result )

#Infinite loop to count score
while True:
    
    #Execute main
    Main()
    #Inform user of their score
    print("You have won {} times. The computer has won {} times".format(playerWins, computerWins))